Quick and dirty script to control backlight (aka screen brightness) on a laptop
where the 'special keys' don't work, like in my case on Manjaro (see 

[forum]: https://forum.manjaro.org/t/backlight-again-problem/43085/42	"forum)"

Put in some visible path the scipt, then bind to two key combinations (assuming
the script name is BACKLIGHT):

BACKLIGHT inc (for incrementing)
BACKLIGHT dec (for decrementing)

No checking of commands!! so if something is not installed this will just fail

Needs installed:
- Python 3
- xbacklight (in extra/xorg-xbacklight)
- volnoti (community/volnoti)
